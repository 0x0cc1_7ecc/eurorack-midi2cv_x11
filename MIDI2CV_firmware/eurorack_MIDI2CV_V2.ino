//
//  10/11/2021 : TBD :
//      -test +fix clock
//      -ajouter mode polyphonique
//      -validate COM_RX: mode, offset,clock,edit output
//      - port all modes to MIDI_MAP mode (array copy)
//      - up the MIDI channel MIDI_MAP mode to 16
//
//
//

#include <SPI.h>
#include <SoftwareSerial.h>
#include <MCP48xx.h>
#include <MIDI.h>

//MIDI_CREATE_INSTANCE(HardwareSerial, Serial1, MIDI);
MIDI_CREATE_DEFAULT_INSTANCE();
MCP4822 dac(2);
MCP4822 dac1(3);
MCP4822 dac2(4);
MCP4822 dac3(5);
MCP4822 dac4(6);


const boolean debug=0;
int mode=0;
int volt=1000;
int convMode = 0;

// PINS //
const int GTCLK_PINS[]={14};//5,6,7,8
const int MIDILED_PIN=7;
const int CLOCKLED_PIN=8;
const int MODE_PIN=A3;
const int RX328_PIN=9;
const int TX328_PIN=10;

//LED COLORS //
enum color {OFF,RED,GREEN,BLUE,YELLOW,VIOLET,ORANGE,PINK};
enum ledI {CLOCK,CLOCKDIV,IN1,IN2,RXMIDI,IN3};// TEMPORARY AS RGB LED 4 MISSING

//VALUES & BUFFERS
int GCo=0b00;
int GCo_b=0b00; // CLOCK0,CLOCK1
int CCn[]={10,11,10,11,10,11,10,11,10,11};
byte midi_rx[]={0,0,0};
byte midi_buf[]={0,0,0};
boolean ledm_b=0;
boolean ledc_b=0;
const int clockdur=47,clockend=95;
int clockddur=47;
int ticks=0;
int clockD=2;
boolean clockDs=1;//0 = clock multiply, 1= clock divide
int clocks=0;
word volts=0;
byte MIDI_map_maxch=8;
int l=512;

struct CVOUT {
  int value;
  int buff;
  int ofst;
  int pin;
};

CVOUT cvs[]={
//    value buffer  offset  pin
     {0,    0,      320,      2},
     {0,    0,      320,      2},
     {0,    0,      320,      3},
     {0,    0,      320,      3},
     {0,    0,      320,      4},
     {0,    0,      320,      4},
     {0,    0,      320,      5},
     {0,    0,      320,      5},
     {0,    0,      320,      6},
     {0,    0,      320,      6}
};

byte MIDI_map[][40]={ // GATE PITCH VEL CC1 CC2    
 //CH0            CH1            CH2            CH3            CH4            CH5            CH6            CH7
  {0 ,1 ,2 ,3 ,4 ,5 ,6 ,7 ,8 ,9 ,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99},// 2 CH * 5 CV/GATE
  {0 ,5 ,99,99,99,1 ,6 ,99,99,99,2 ,7 ,99,99,99,3 ,8 ,99,99,99,4 ,9 ,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99},// 5 CH * 2 CV/GATE
  {0 ,1 ,2 ,3 ,99,5 ,6 ,7 ,8 ,99,4 ,9 ,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99},// 3 CH * 4 CV/GATE + 2*CV/GATE
  {0 ,1 ,2 ,3 ,4 ,5 ,6 ,7 ,8 ,9 ,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99},// EDITABLE 
  {0 ,1 ,2 ,3 ,4 ,5 ,6 ,7 ,8 ,9 ,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99}, //SYNTH
  {0 ,1 ,2 ,3 ,4 ,5 ,6 ,7 ,8 ,9 ,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99}  //CALIBRATION
};

unsigned int table[512];
byte waveptr=0;
void syn_table_init(int shape=2) {
  waveptr=0;
  if (shape == 1) { // TRI
    int incr=4000/(l/4);
    int v1=0;
      for (int i=0;i<l;i++) {
        if (i<l/4) {
        v1=v1+incr;
        } else {
        v1=v1-incr;
        }
        table[i]=v1;
  }
    
  } else if (shape == 0) { // SINE 
    for (int i=0;i<l;i++) {
        table[i]=int(2000.0+2000.0*sin(2*PI*float(i)*0.0039)); //0.09*0.087*0.5 ou 0.0039
    }
  } else if (shape == 2) {// SQUARE
      for (int i=0;i<l;i++) {
              if (i<l/4) {
                table[i]=4000;
              } else {
                table[i]=1;
              }
        
      }
  } else if (shape == 3) { // DISTORTED SINE
    for (int i=0;i<l;i++) {
        table[i]=int(2000.0+2000.0*sin(2*PI*float(i)*0.0039)*(1+0.4*sin(2*PI*float(i)*0.09*0.087))); //0.09*0.087*0.5
        //table[i]=int(wv)*15;
    }
   

  }else if (shape == 4) { // MUlti SINE    
    for (int i=0;i<l;i++) {
        table[i]=int(2000.0+1500.0*sin(2*PI*float(i)*0.09*0.087)*(1+0.5*sin(3*PI*float(i)*0.0039))); //0.09*0.087*0.5
    }
  } else if (shape == 5) { // EPROM
    for (int i=0;i<l;i++) {
       // table[i]=int(EEPROM.Read()*15.68);
    }
  }
  
}

SoftwareSerial Serial328(RX328_PIN, TX328_PIN); // RX, TX

/////////////////////////////// PROTOCOL ///////////////////////////////////////////////////////////////////////////////////////////////
//
//    MSG_NAME  SIZE   MSG#   | DATA
//    ------------------------|------------------------------------------------------------------------------------------------------------------------------------  
//    STATUS_ALL  15   0xFF   | CV1_OUT | GATE1_OUT| CV2_OUT | CV3_OUT |  CLK_OUT |  CV4_OUT | GATE2_OUT | CV5_OUT | CV6_OUT | CLKD_OUT|  CV1_IN | CV2_IN | CV3_IN |
//    STATUS_OUTS 12   0xF0   | CV1_OUT | GATE1_OUT| CV2_OUT | CV3_OUT |  CLK_OUT |  CV4_OUT | GATE2_OUT | CV5_OUT | CV6_OUT | CLKD_OUT|
//    STATUS_INS  05   0xF1   | CV1_IN  | CV2_IN   | CV3_IN  |
//    STATUS_OUT  04   0xF2   | CV_OUT# | CV_OUT#V |
//    STATUS_MIDI ##   0xFE   | (depends on midi msg type)
//    LED_CONTROL 06   0xFD   | LED_#   |  LED_R   |  LED_G  | LED_B   |
//                            |         
//    MODE_UPDT   03   0xD0   | MODE#   |
//    MODE_READ   03   0xD1   | MODE#   |
//    MODE_PRMR   04   0xD2   | PARAM#  | PARAM#V |
//    MODE_PRMW   04   0xD3   | PARAM#  | PARAM#V |
//
//    PARAMETER NAME           PARAM#   SIZE(BYTES)
//    Main module mode              0            1 
//    MIDI Conversion mode          1            1
//    Clock multiplier              2            1
//    CV offset chan               10            4      format : channel# value   
//    MIDI Custom mapping          20            2      format : midi param# value
//    MIDI Custom mapping max ch   21            1 


void COM_write(byte msg[],byte msgName) {
  
  Serial328.write(sizeof(msg)+2);
  Serial328.write(msgName);
  Serial328.write(msg,sizeof(msg));
  
   
}

void COM_RX() {
  if (Serial328.available()) {
    byte rx_msg[5];
    int j=0;
    while (Serial328.available() != 0) {
      rx_msg[j]=Serial328.read();
      delay(2);
      //Serial.println();
      
      j=j+1;
      
    }   
    /*for(int i=0;i<sizeof(rx_msg);i++) {
      //Serial.print(String(rx_msg[i])+" ");
      COM_clockled(1);delay(100);COM_clockled(0);delay(100);
    }*/
    //Serial.println("");
  
    if ( ( rx_msg[1] == 0xD3) && (rx_msg[0] < 13) ) {
      /// PARAMETER CHANGE
      
      COM_clockled(1);delay(500);COM_clockled(0);
      switch (rx_msg[2]) {     
        case 0://main mode
        if (rx_msg[3]<sizeof(MIDI_map)/sizeof(MIDI_map[0]) ) {convMode=rx_msg[3];mode=rx_msg[3];}
        if (rx_msg[3]==5) {COM_midiled(1);} else {COM_midiled(0);}
        //Serial.println("modechange");
        break;
        case 1://midi conversion mode
        convMode=rx_msg[3];mode=convMode;
        break;
        case 2://clock multiplier
        //clockV[]={-4,-2,1,2,3,4,8,16,24};
        switch (rx_msg[3]) {case 0: clockDs=1;clockD=4;break;  case 1: clockDs=1;clockD=2;break;   case 2:clockDs=0;clockD=1;break;    case 3:clockDs=0;clockD=2;break;    case 4:clockDs=0;clockD=3;break;
                       case 5: clockDs=0;clockD=4;break;  case 6: clockDs=0;clockD=8;break;   case 7: clockDs=0;clockD=16;break;  case 8: clockDs=0;clockD=24;break;}
        clockddur=int(clockD*(clockend+1)/2)-1;
        if (clockDs) {clockddur=int((clockend+1)/(2*clockD));}
        //Serial.println("clockchange");
        break;
        case 10://CV channel offset 
        cvs[rx_msg[3]].ofst=map(rx_msg[4],0,255,0,500);//Serial.println("offsetchange");
        if (mode==4) {l=map(rx_msg[4],0,255,0,512);syn_table_init();}
        if (mode==5) {CV_update(rx_msg[3],map(cvs[rx_msg[3]].ofst,0,255,1,7000));}
        break;
        case 20://MIDI Custom mapping
        if (rx_msg[3]<81) {
          MIDI_map[convMode][rx_msg[3]]=rx_msg[4];
        } else   {
          if ( (rx_msg[4]>=0) && (rx_msg[4]< 5) ){
            syn_table_init(rx_msg[4]);
          }
        }
        break;
        case 21://MIDI Custom mapping max ch  
        MIDI_map_maxch=rx_msg[3];
        break;
        
      }
    }
  }
}

void COM_TXio() {
  //byte msg[]={byte(CVo[0]/62),byte(bitRead(GCo,0)),byte(CVo[1]/62),byte(CVo[2]/62),byte(bitRead(GCo,2)),   byte(CVo[3]/62),byte(bitRead(GCo,1)),byte(CVo[4]/62),byte(CVo[5]/62),byte(bitRead(GCo,3)),10,10,10};
  //COM_write(msg,0xFF);
}

void COM_TXmidi() {
  //byte msg[]={midi_rx[0],midi_rx[1],midi_rx[2]};
  byte msg[]={80,MIDI.getData1(),MIDI.getData2()};
  COM_write(msg,0xFE);
}
void COM_clockled(boolean state) {
  if (state != 0) { PORTB &=~(1<<CLOCKLED_PIN-4);} else { PORTB |=(1<<CLOCKLED_PIN-4);}
  ledc_b=state;
}
void COM_midiled(boolean state) {
  
  if (state != 0) { PORTE &=~(1<<MIDILED_PIN-1);} else { PORTE |=(1<<MIDILED_PIN-1);}
  ledm_b=state;
  //byte ledfrm[6]={5,0xFD,4,0,0,255*byte(state)};
 // ledm_b=state;
  
 // Serial328.write(ledfrm,sizeof(ledfrm));
}

void COM_led(enum ledI led,enum color c,boolean state) { 

  byte r,g,b;
    
  switch (c) {
    case OFF:     r=000;g=000;b=000;break;
    case RED:     r=255;g=000;b=000;break;
    case GREEN:   r=000;g=255;b=000;break;
    case BLUE:    r=000;g=000;b=255;break;
    case YELLOW:  r=255;g=255;b=000;break;
    case VIOLET:  r=255;g=000;b=255;break;
    case ORANGE:  r=255;g=100;b=000;break;
    case PINK:    r=255;g=000;b=050;break;
    
  }

  byte ledfrm[6]={5,0xFD,led,byte(state)*g,byte(state)*r,byte(state)*b};
  ledm_b=state;
  
  Serial328.write(ledfrm,sizeof(ledfrm));
}

/////////////////////////////////////////     MIDI     /////////////////////////////////////////////////////////////////////////////////////////////

void MIDI_receive () {
   if (MIDI.read() ) {
    int ch=0;
    if ( (convMode <4)  ) {
       ////////  4 MIDI CLASSIC CONV MODES   ////////////////////////////////////////////////
            switch(MIDI.getType()) {
           case midi::NoteOn:
                ch=MIDI.getChannel()-1;
                if (ch<MIDI_map_maxch) {
                  if (MIDI_map[convMode][ch*5+1] != 99 ) {CV_update(MIDI_map[convMode][ch*5+1],CV_curve(3,MIDI.getData1()) );} // V/OCT
                  if (MIDI_map[convMode][ch*5] != 99 ) {CV_update(MIDI_map[convMode][ch*5],7000 ); }                     // GATE
                  if (MIDI_map[convMode][ch*5+2] != 99 ) {CV_update(MIDI_map[convMode][ch*5+2],MIDI.getData2()*62 ); }       // VELOCITY
                  COM_midiled(1);
                }              
                 
           break;
           case midi::NoteOff:
                ch=MIDI.getChannel()-1;
                 if (ch<MIDI_map_maxch) {
                  if (MIDI_map[convMode][ch*5+1] != 99 ) {CV_update(MIDI_map[convMode][ch*5+1],CV_curve(3,MIDI.getData1()) );} // V/OCT
                  if (MIDI_map[convMode][ch*5] != 99 ) {CV_update(MIDI_map[convMode][ch*5],0 ); }                     // GATE
                  if (MIDI_map[convMode][ch*5+2] != 99 ) {CV_update(MIDI_map[convMode][ch*5+2],MIDI.getData2()*62  ); }       // VELOCITY 
                  COM_midiled(0);
                }      
                
           break;
           case midi::ControlChange:
                ch=MIDI.getChannel()-1;
                if (ch < MIDI_map_maxch) {
                  if (MIDI.getData1() == CCn[ch*2]) {
                    CV_update(MIDI_map[convMode][ch*5+3],MIDI.getData2()*62);         // CC1
                  } else if (MIDI.getData1() == CCn[ch*2+1]) {
                    CV_update(MIDI_map[convMode][ch*5+4],MIDI.getData2()*62);         // CC2
                  }
                }
           break;
           case midi::Start:
            ticks=0;clocks=0;
           break;
           case midi::Clock: ////////  CLOCK   ////////////////////////////////////////////////
              if (clockDs) { // CLOCK DIVIDE ( /2 & /4 )
                  for (int i=0;i<clockD;i++) {
                      if      (ticks==int( (2*i+0)*clockend/(2*clockD)) ) {bitWrite(GCo,0,1);}
                      else if (ticks==int( (2*i+1)*clockend/(2*clockD)) ) {bitWrite(GCo,0,0);}
                  }   
              } else { // CLOCK MULTIPLY
                for (int i=0;i<clockD;i++) {
                      if      (clocks % clockD==0 ) {bitWrite(GCo,0,1);}
                      else if (clocks*(clockend+1)+ticks==round( (clockend+1)*clockD/2) ) {bitWrite(GCo,0,0);}
                  } 
              }
              
              if      (ticks==0) {bitWrite(GCo,1,1);}  // REGULAR CLOCK
              else if (ticks == int(clockend/2)) {bitWrite(GCo,1,0);} 
              
              if (ticks != clockend) {ticks++;} 
              else {ticks=0;clocks++;}
              
              if (clocks==clockD) {clocks=0;}

              CLOCK_manage();
           break;          
        }
    }   
    
   
   } else {
      if (ledm_b) {
      }
    
   }
  
}


/////////////////////////////////////////     CV/GATE OUT     /////////////////////////////////////////////////////////////////////////////////////////////

int noteV[128] = {/*
C      C#    D   D#    E    F    F#    G    G#   A    A#   B */
0,      85,  165, 253, 340, 422,  508, 593, 670, 757, 844, 925,
1010,  1094, 1176,1264,1350,1426,1516,1599,1677,1765,1850,1929,
2015,  2100, 2180,2269,2355,2436,2525,2612,2687,2776,2859,2944,
3032,  3115, 3196,3285,3368,3449,3540,3618,3699,3790,3871,3953,
4043,  4124, 4206,4297,4376,4459,4549,4628,4707,4797,4877,4960,
5050,  5128, 5211,5299,5380,5461,5554,5629,5710,5802,5877,5957,
6047,  6128, 6209,6300,6379,6460,6548,6624,6702,6791,6870,6946,
7037,  7114, 7191,7283,7357,7435,7518,7600,7674,7674,7674,7674,
7674,  7674, 7674,7674,7674,7674,7674,7674,7674,7674,7674,7674,
7674,  7674, 7674,7674,7674,7674,7674,7674,7674,7674,7674,7674
};


int CV_curve(int curve,int value) {
  switch (curve) {

    case 0://CV note curve  
        return value*62; 
    break;

    case 1://VEL curve
      return value*20;
    break;

    case 2:// CC curve
       return value*63;
    break;

    case 3://lookup table
      return noteV[value];
    break;
     
  }
}

void CLOCK_manage() {
  int toto=0;// 0= clockdiv,1=clock
  if (clockD==1){toto=1;}
  for (int i=toto;i<sizeof(GTCLK_PINS)+toto;i++) {
        if (bitRead(GCo,i) != bitRead(GCo_b,i)) {
          if (bitRead(GCo,i) != 0) { PORTB &=~(1<<3);} else { PORTB |=(1<<3);} 
          bitWrite(GCo_b,i,bitRead(GCo,i));  
          COM_clockled(bitRead(GCo,toto));   
        }
     //COM_led(i,YELLOW,bitRead(GCo,toto));     
    }
      
}

void CV_update(int chan,int value) {
  
  volts=map(value+cvs[chan].ofst,0,8000,0,4095); 
  switch (chan) {
  
       case 0: // GATE1
          PORTD &=~(1<<1); //CS_MCP LOW (EN)
          SPDR=highByte(volts)| B00010000; while (!(SPSR&(1<<SPIF))); // Gainx2
          SPDR=lowByte(volts); while (!(SPSR&(1<<SPIF))); 
          PORTD |=(1<<1);  // CS_MCP HIGH (DIS)
       break;
       case 1: //NOTE1
          PORTD &=~(1<<1); //CS_MCP LOW (EN)
          SPDR=highByte(volts)| B10010000; while (!(SPSR&(1<<SPIF))); // Gainx2
          SPDR=lowByte(volts); while (!(SPSR&(1<<SPIF)));        
          PORTD |=(1<<1);  // CS_MCP HIGH (DIS)
       break;
       case 3: //VEL1
          PORTD &=~(1<<0); //CS_MCP LOW (EN)
          SPDR=highByte(volts)| B00010000; while (!(SPSR&(1<<SPIF))); // Gainx2
          SPDR=lowByte(volts); while (!(SPSR&(1<<SPIF)));
          PORTD |=(1<<0);  // CS_MCP HIGH (DIS)
       break;
       case 2: //CC1
          PORTD &=~(1<<0); //CS_MCP LOW (EN)
          SPDR=highByte(volts)| B10010000; while (!(SPSR&(1<<SPIF))); // Gainx2
          SPDR=lowByte(volts); while (!(SPSR&(1<<SPIF)));
          PORTD |=(1<<0);  // CS_MCP HIGH (DIS)
       break;
       case 4: //CC11
          PORTD &=~(1<<4); //CS_MCP LOW (EN)
          SPDR=highByte(volts)| B00010000; while (!(SPSR&(1<<SPIF))); // Gainx2
          SPDR=lowByte(volts); while (!(SPSR&(1<<SPIF)));
          PORTD |=(1<<4);  // CS_MCP HIGH (DIS)
       break;
       case 5: //GATE2
          PORTD &=~(1<<4); //CS_MCP LOW (EN)
          SPDR=highByte(volts)| B10010000; while (!(SPSR&(1<<SPIF))); // Gainx2
          SPDR=lowByte(volts); while (!(SPSR&(1<<SPIF)));
          PORTD |=(1<<4);  // CS_MCP HIGH (DIS)
       break;
       case 6: //CV2
          PORTC &=~(1<<6); //CS_MCP LOW (EN)
          SPDR=highByte(volts)| B00010000; while (!(SPSR&(1<<SPIF))); // Gainx2
          SPDR=lowByte(volts); while (!(SPSR&(1<<SPIF)));
          PORTC |=(1<<6);  // CS_MCP HIGH (DIS)
       break;
       case 7: //VEL2
          PORTC &=~(1<<6); //CS_MCP LOW (EN)
          SPDR=highByte(volts)| B10010000; while (!(SPSR&(1<<SPIF))); // Gainx2
          SPDR=lowByte(volts); while (!(SPSR&(1<<SPIF)));
          PORTC |=(1<<6);  // CS_MCP HIGH (DIS)
       break;
       case 8: //CC2
          PORTD &=~(1<<7); //CS_MCP LOW (EN)
          SPDR=highByte(volts)| B00010000; while (!(SPSR&(1<<SPIF))); // Gainx2
          SPDR=lowByte(volts); while (!(SPSR&(1<<SPIF)));
          PORTD |=(1<<7);  // CS_MCP HIGH (DIS)
       break;
       case 9: //CC22
          PORTD &=~(1<<7); //CS_MCP LOW (EN)
          SPDR=highByte(volts)| B10010000; while (!(SPSR&(1<<SPIF))); // Gainx2
          SPDR=lowByte(volts); while (!(SPSR&(1<<SPIF)));     
          PORTD |=(1<<7);  // CS_MCP HIGH (DIS)
       break;
        
      }
}

void CV_manage() { 
  for (int i=0;i<sizeof(cvs)/sizeof(cvs[0]);i++) {
    if (cvs[i].value != cvs[i].buff ) {  
      CV_update(i,cvs[i].value);
      cvs[i].value = cvs[i].buff;  
    }
  }
}

void CV_zero() {
  for (int i=0;i<sizeof(cvs)/sizeof(cvs[0]);i++) {
    cvs[i].value=1;
  }
  CV_manage();
}

void CV_max() {
  for (int i=0;i<sizeof(cvs)/sizeof(cvs[0]);i++) {
    cvs[i].value=8000;
  }
  CV_manage();
}

void CV_mid() {
  for (int i=0;i<sizeof(cvs)/sizeof(cvs[0]);i++) {
    cvs[i].value=4000;
  }
  CV_manage();
}

void CV_set_all(int val) {
  for (int i=0;i<sizeof(cvs)/sizeof(cvs[0]);i++) {
    cvs[i].value=val;
  }
  CV_manage();
}

void CV_calib(int calmode) {
  // calmode = 0 test, 1 = 4096 test
  
  if (calmode == 0) {
      CV_zero();
  }
  if (calmode == 1) {
      CV_max();
  }
  if (calmode == 2) {
      CV_mid();
  }
}
///////////////////////////////////////// WAVEFORMS ////////////////////////////////////////////////////////////////////////////////////////////////////////


void syn_table(byte chan=0) { 
  CV_update(chan,map(table[waveptr],0,4000,0,7000));
  if (waveptr<l-1) {waveptr++;}
  else {waveptr=0;}
}


/////////////////////////////////////////    MODULES SETUP     /////////////////////////////////////////////////////////////////////////////////////////////

void DAC_setup() {
  SPI.begin();
  SPI.setClockDivider(4);     // defaut : 4. peut être 2,4,8,16,32,64,128
  SPI.setDataMode(SPI_MODE0); // mode: SPI_MODE0, SPI_MODE1, SPI_MODE2, or SPI_MODE3
  for (int i=0;i<(sizeof(cvs)/sizeof(cvs[0]))/2;i=i+2) {
    pinMode(cvs[i].pin,OUTPUT);
    digitalWrite(cvs[i].pin,HIGH);  // Desactive le chip
  }
  dac.init();
  dac1.init();
  dac2.init();
  dac3.init();
  dac4.init();
  
  dac.turnOnChannelA();
  dac.turnOnChannelB();
  dac.setGainA(MCP4822::High);
  dac.setGainB(MCP4822::High);
  
  dac1.turnOnChannelA();
  dac1.turnOnChannelB();
  dac1.setGainA(MCP4822::High);
  dac1.setGainB(MCP4822::High);
  
  dac2.turnOnChannelA();
  dac2.turnOnChannelB();
  dac2.setGainA(MCP4822::High);
  dac2.setGainB(MCP4822::High);

  dac3.turnOnChannelA();
  dac3.turnOnChannelB();
  dac3.setGainA(MCP4822::High);
  dac3.setGainB(MCP4822::High);

  dac4.turnOnChannelA();
  dac4.turnOnChannelB();
  dac4.setGainA(MCP4822::High);
  dac4.setGainB(MCP4822::High);
}

void GATECLK_setup () {
  for (int i=0;i<sizeof(GTCLK_PINS);i++) {
    pinMode(GTCLK_PINS[i],OUTPUT);

  }
}

void COM_setup() {
  pinMode(MIDILED_PIN,OUTPUT);
  pinMode(CLOCKLED_PIN,OUTPUT);
  Serial328.begin(9600);
  Serial328.setTimeout(20);
  
  //pinMode(A3,INPUT_PULLUP);
  digitalWrite(MIDILED_PIN,HIGH);
  digitalWrite(CLOCKLED_PIN,HIGH);
}


/////////////////////////////////////////     SETUP & MAIN     /////////////////////////////////////////////////////////////////////////////////////////////   

void setup() {
  
  DAC_setup();
  GATECLK_setup();
  COM_setup();
  MIDI.begin(MIDI_CHANNEL_OMNI);
  //Serial.begin(9600);
  syn_table_init();
}

void loop() {

  if (!debug) {
    
    MIDI_receive();
    COM_RX();

    if ( (mode ==4)  ) {
      syn_table();
    }
    if ( (mode ==5)  ) {
      //CV_calib(2);
    }
    

  } else {
    
  
  }  
}
