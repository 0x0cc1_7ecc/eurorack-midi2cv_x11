//
//    EURORACK_GUI
//
//    0x0cc173ccF0c5a113 
//
//    eurorack 3 in / 8 out w/ MIDI support GUI firmware (arduino nano 328p) 
//    V2.0: 11/11/2021 update:
//                        - implementation on arduino Nano with MIDI2CV V2 board (5*12 bit DACs)
//                        - 2 leds conf OK but led 0 controls also led 1
//    V0.1: 01/08/2021 creation
//                     still to do:
//                        -handle EEPROM write errors
//                        -fill EEPROM + implement strings from EEPPROM (functions are ready)
//                        -implement operational mode handling via UART
//                        -optimize screen refresh more
//                        -implement protocol on arduino pro micro (main board)
//
//
//
//
//
//
//

#include "TFT_22_ILI9225.h"
#include <Adafruit_NeoPixel.h>

#include <EEPROM.h>

#include <../fonts/FreeSans24pt7b.h>
#define TFT_BRIGHTNESS 200
#define TFT_RST 5
#define TFT_RS  8    //OC
#define TFT_CS  4  // SS
#define TFT_SDI 11 // MOSI
#define TFT_CLK 13  // SCK
#define TFT_LED 7   // 0 if wired to +5V directly
const int ledPin=6;
const int midiled_pin=A4,clockled_pin=A2;
const int ledCount=2;

int debugTime=200;
boolean led_enable=0;
boolean debug=0;

int action=98;
int guinit=1;
int lastAction=0;
byte msg=0x00, note=0x00, vel=0x00,bmsg=0x00,bnote=0x00,bvel=0x00;
int CVo_ofst[]={0,0,0 ,0,0,0};
byte CV_outs[]={0,0,0  ,0,0,0};
int tup=0;
unsigned long tp=0;

struct CV_OUT{
  byte ch;
  byte msg;//0=NOTE,1=GATE,2=VEL,3=CC,4=CLK,5=SINE,6=TRI,7=SQR,8=CAL
  int ofst;
  byte rsrvd;
};


CV_OUT CV_OUTS[][10]={
  //ch msg ofst rsvrd
  { {0,1,320,0},  {0,0,320,0},{0,2,320,0},{0,3,320,10},{0,3,320,11},
    {1,1,320,0},  {1,0,320,0},{1,2,320,0},{1,3,320,10},{1,3,320,11} },// mode 0 : 2CH x 5CV
    
  { {0,1,320,0},  {1,1,320,0},{2,1,320,0},{3,1,320,0},{4,1,320,0},
    {0,0,320,0},  {1,0,320,0},{2,0,320,0},{3,0,320,0},{4,0,320,0}   },// mode 1 : 5CH x 2CV
    
  { {0,1,320,0},  {0,0,320,0}, {0,2,320,0},{0,3,320,10}, {2,1,320,0},
    {1,1,320,0},  {1,0,320,0}, {1,2,320,0},{1,3,320,10}, {2,0,320,0} },// mode 2 : 3CH x 4/2CV
    
  { {0,1,320,0},  {1,1,320,0},{2,1,320,0},{3,1,320,0},{4,1,320,0},
    {0,0,320,0},  {1,0,320,0},{2,0,320,0},{3,0,320,0},{4,0,320,0}   },// mode 3 : custom (here 5CH x 2CV)
    
  { {0,5,440,0},  {1,6,440,0},{2,7,440,0},{0,5,440,10},{1,6,440,00},
    {0,5,440,0},  {1,6,440,0},{2,7,440,0},{0,5,440,10},{1,6,440,00} },// mode 4 : SYNTH
    
  { {0,10,4000,0},  {0,10,4000,0},{0,10,4000,0},{0,10,4000,10},{0,10,400,0},
    {0,10,4000,0},  {0,10,4000,0},{0,10,4000,0},{0,10,4000,10},{0,10,400,0}   }// mode 5 : CALIBRATE
};


int mode=0,modeC=5,convMode=0,convModeC=4,clockD=0,clockDC=15;
signed int clockV[]={-4,-2,1,2,3,4,8,16,24};
byte cur=0,cur_b=0,m_size,cv=0,cvC=5,cbox=0;
unsigned long int blk=0;
boolean blkC=0;
int x0=10,y0=10,xm,ym;


TFT_22_ILI9225 tft = TFT_22_ILI9225(TFT_RST, TFT_RS, TFT_CS, TFT_LED, TFT_BRIGHTNESS);

Adafruit_NeoPixel strip(ledCount, ledPin, NEO_GRB + NEO_KHZ800);


/////////////////////////////// GUI CONTROLS ///////////////////////////////////////////////////////////////////////////////////////////////
struct ctls{
  int pin1;
  int pin2;
  int type;// 0 = push button,1=encoder
  int mode;// push : 0= toggle, 1=momentary \\ encoder : 0= up/down
  int val;
  int bval;// buffer
};

ctls ctl[] = {  //pin1  pin2  typ  mode    val     bval
{/*encoder 1 push*/   3   ,3   ,0    ,0    ,HIGH   ,HIGH  },
{/*encoder 2 push*/   2   ,2   ,0    ,0    ,HIGH   ,HIGH  },
{/*encoder 3 push*/   9   ,9   ,0    ,0    ,HIGH   ,HIGH  }
};

void encoder_read(int n) {
  if (!digitalRead(ctl[n].pin1)) {
     if (digitalRead(ctl[n].pin2)) {        
           //UP!
           action=1;
            if (debugTime!=0) {delay(debugTime);}
          }
           else {           
           //DOWN!!
           action=2;
           }
    }
}

void push_read(int n) {
  bool rd=digitalRead(ctl[n].pin1);
  if ( (!rd) && (rd != ctl[n].bval) ) {
    ctl[n].bval=rd; 
    if ( n == 0) { action=1;}
    if ( n == 1) { action=2;}
    if ( n == 2) { int top=millis()+800;action=3;while(!digitalRead(ctl[n].pin1)){delay(50);} if (millis() > top) {action=4;} }
    //action=3;
    //tft.setFont(Terminal6x8);tft.drawText(0,0,"  ");tft.drawText(0,0,String(action));
  } else if ( (rd) && (rd != ctl[n].bval) ) {
    ctl[n].bval=rd; 
  }
  
}

void controls_read() {
  //encoder_read(2);
  push_read(0);
  push_read(1);
  push_read(2);
}

void controls_init() {
  for (int i=0;i<sizeof(ctl)/sizeof(ctl[0]);i++) {
    if (ctl[i].type==0) {
      pinMode (ctl[i].pin1, INPUT_PULLUP);
    }
  }
  pinMode(midiled_pin,INPUT_PULLUP);
  pinMode(clockled_pin,INPUT_PULLUP);
}


/////////////////////////////// MIDI ///////////////////////////////////////////////////////////////////////////////////////////////

void MIDI_read() {
    //msg=0xB1;note=0x50;vel=0x06;// ch1 cc# 50 val 6
    //msg=0x90;note=0x00;vel=0x7F;// ch0 note 26 vel 127

    if (Serial.available()==3) {
       msg=Serial.read();
       note=Serial.read();
       vel=Serial.read();

      
    }
}
/////////////////////////////// EEPPROM ///////////////////////////////////////////////////////////////////////////////////////////////
int EE_start_add=10;

void EEPROM_dump() { 
  for (int i=0;i<EEPROM.length()+1;i++) {
    if (i % 10 == 0) {
      Serial.print(String(int(i))+" :");
      for (int j=0;j<10;j++) {
          Serial.print(String(EEPROM.read(i+j))+" ");
      }
      Serial.println();  
    }   
  }
}

void EEPROM_dump_str() {
  for (int i=0;i<EEPROM.length()+1;i++) {
    if (i % 10 == 0) {
      Serial.print(String(int(i))+" :");
      for (int j=0;j<10;j++) {
          if ( (EEPROM.read(i+j) <32) || (EEPROM.read(i+j) >127) || (EEPROM.read(i+j) ==0) || i+j == EE_start_add ){
            Serial.print(String(EEPROM.read(i+j))+" "); 
          } else {
            Serial.print(String( ( (char) EEPROM.read(i+j) )) +"   " );
          }
      }
      Serial.println();  
    }  
  }
}

void EEPROM_dump_strlist() {
  int addr=20;
  int line=10;
  tft.setFont(Terminal6x8);
  while (addr<EEPROM.read(EE_start_add)) { 
   //Serial.println(String(addr)+" : "+EEPROM_read_string(addr));
    tft.drawText(10, line, String(addr)+" : "+EEPROM_read_string(addr));
    addr=addr+EEPROM.read(addr)+2;
    line=line+10;  
  }
}

void EEPROM_erase(int from,int to) {
  for (int i=0;i<to-from+1;i++) {
    EEPROM.write(from+i,255);
  }
}

void EEPROM_write_string(String str) {
  
  int EE_end_add=EEPROM.read(EE_start_add); 
  EEPROM.write(EE_start_add,EE_end_add+str.length()+2);
  EEPROM.write(EE_end_add,str.length());
  for (int i=0;i<str.length();i++) {
    EEPROM.write(EE_end_add+1+i,int(str.charAt(i)) );
  }
 EEPROM.write(EE_end_add+1+str.length(),0);

}

String ESTR(int addr) {
  return EEPROM_read_string(addr);
}

String EEPROM_read_string(int addr) {
  int l=EEPROM.read(addr);
  String s;
  for (int i=0;i<l;i++) {
    s=s+String((char) EEPROM.read(addr+i+1));
  }
  return s;
}


void EEPROM_init() {
  //Serial.println(EEPROM_read_string(20));
  //EEPROM_erase(20,800);
  //EEPROM.write(EE_start_add,20);
  //EEPROM_write_string("CACA");
  
 
}

/////////////////////////////// PROTOCOL ///////////////////////////////////////////////////////////////////////////////////////////////

//
//    MSG_NAME  SIZE   MSG#   | DATA
//    ------------------------|------------------------------------------------------------------------------------------------------------------------------------  
//    STATUS_ALL  15   0xFF   | CV1_OUT | GATE1_OUT| CV2_OUT | CV3_OUT |  CLK_OUT |  CV4_OUT | GATE2_OUT | CV5_OUT | CV6_OUT | CLKD_OUT|  CV1_IN | CV2_IN | CV3_IN |
//    STATUS_OUTS 12   0xF0   | CV1_OUT | GATE1_OUT| CV2_OUT | CV3_OUT |  CLK_OUT |  CV4_OUT | GATE2_OUT | CV5_OUT | CV6_OUT | CLKD_OUT|
//    STATUS_INS  05   0xF1   | CV1_IN  | CV2_IN   | CV3_IN  |
//    STATUS_OUT  04   0xF2   | CV_OUT# | CV_OUT#V |
//    STATUS_MIDI ##   0xFE   | (depends on midi msg type)
//    LED_CONTROL      0xFD   | LED_#   |  LED_R   |  LED_G  | LED_B   |

//                            |         
//    MODE_UPDT   03   0xD0   | MODE#   |
//    MODE_READ   03   0xD1   | MODE#   |
//    MODE_PRMR   04   0xD2   | PARAM#  | PARAM#V |
//    MODE_PRMW   04   0xD3   | PARAM#  | PARAM#V |
//


void COM_read() {
  if ( Serial.available() ) {
    byte siz=Serial.read();  
    if (siz < 20) {
      byte rx_msg[siz];
      tft.drawText(10,0,"                                      ");
      tft.drawText(10,0,String(siz));
      for (int i=0;i<sizeof(rx_msg);i++) {
        rx_msg[i]=Serial.read();
       
      }
      
       for (int i=0;i<sizeof(rx_msg);i++) {
         tft.drawText(20+20*i,10,String(rx_msg[i],HEX));
       }
      action=4;
      switch(rx_msg[0]) {
        case 0xFF:
          for (int i=0;i<sizeof(CV_outs);i++) {
            CV_outs[i]=rx_msg[i+1];
          }
          
        break;
        case 0xF0:
          for (int i=0;i<sizeof(CV_outs);i++) {
            CV_outs[i]=rx_msg[i+1];
          }
        break;
        
        case 0xF2:
          CV_outs[rx_msg[1]]=rx_msg[2];
          action=50+rx_msg[1];
        break;
        case 0xFE:
          msg=rx_msg[1];note=rx_msg[2];vel=rx_msg[3];
        break;
      
        case 0xFD:
         // correction strip.setPixelColor( rx_msg[1],rx_msg[2],rx_msg[3],rx_msg[4]);
          strip.setPixelColor( rx_msg[1],rx_msg[2],rx_msg[3],rx_msg[4]);
          strip.show();
        break;
        
        case 0xD0:
        break;
        case 0xD1:
        break;
        case 0xD2:
        break;
        case 0xD3:
        break;
  
      }
    
   } 
  }
  if (!digitalRead(midiled_pin)) {
    strip.setPixelColor(0,000,000,255);strip.show();
  } else {
    strip.setPixelColor(0,000,000,000);strip.show();
  }
  if (!digitalRead(clockled_pin)) {
     strip.setPixelColor(1,255,0,0);strip.show();
  } else {
    strip.setPixelColor(1,0,0,0);strip.show();
  }
  
}

void COM_write(byte msg[]) {
  Serial.write(sizeof(msg)+2);
  for (int i=0;i<sizeof(msg);i++) {
    delay(1);
    Serial.write(msg[i]);    
  }
}
void COM_paramWrite(byte param,byte val1,byte val2) {
  if (param==10) {
    byte msg[]= {0x5,0xD3,param,val1,val2};
    Serial.write(msg,5);
  }else {
    byte msg[]= {0x4,0xD3,param,val1};
    Serial.write(msg,4);
  }
  //COM_write(msg);
}

void COM_CVchan(int chan,byte value) {
    //.msg 0=NOTE,1=GATE,2=VEL,3=CC,4=CLK,5=SINE,6=TRI,7=SQR,8=CAL
    byte param=CV_OUTS[mode][cbox].ch*5;
    if (CV_OUTS[mode][chan].msg == 0) { //0:note
      param+=1;
    }
    else if (CV_OUTS[mode][chan].msg == 1) { //1:gate
      param+=0;
    }
    else if (CV_OUTS[mode][chan].msg == 2) {//2:vel
      param+=2;
    }
    else if (CV_OUTS[mode][chan].msg == 3) {//2:cc
      if (CV_OUTS[mode][chan].rsrvd == 10) {param+=3;}
      else {param+=4;}
      
    }
    if (CV_OUTS[mode][chan].msg < 4) {
      byte msg[]= {0x5,0xD3,20,param,value};
      Serial.write(msg,5);
    } else if ( (CV_OUTS[mode][chan].msg > 4)&&(CV_OUTS[mode][chan].msg < 10) ){
      byte msg[]= {0x5,0xD3,20,81,CV_OUTS[mode][chan].msg-5};
      Serial.write(msg,5);
    }
    
 

}

/////////////////////////////// GUI ///////////////////////////////////////////////////////////////////////////////////////////////

int box_xy(int n,boolean x) {
  if (x) {
    if (n<5) {
      return x0+n*(xm+9)/5;
    } else {
      n=n-5;
      return x0+n*(xm+9)/5;
    }
  } else {
    if (n<5) {
      return y0+10;
    } else {
      return y0+60;
    }
  }
}

String str(int par,int value,int rsv=0) {
  if (par == 0) {//0=NOTE,1=GATE,2=VEL,3=CC,4=CLK
    if (value==0) {return "NOTE ";}
    if (value==1) {return "GATE ";}
    if (value==2) {return "VEL  ";}
    if (value==3) {return "CC"+String(CV_OUTS[mode][rsv].rsrvd)+" ";}
    if (value==4) {return "CLK  ";}
    if (value==5) {return "SINE ";}
    if (value==6) {return "TRI  ";}
    if (value==7) {return "SQR  ";}
    if (value==8) {return "DIST ";}
    if (value==9) {return "MSIN ";}
   if (value==10) {return "CAL  ";}
  }
  if (par == 1) {// modes
    if (value==0) {return "2CH x 5CV";}
    if (value==1) {return "5CH x 2CV";}
    if (value==2) {return "3CH x 4/2CV";}
    if (value==3) {return "CUSTOM";}
    if (value==4) {return "SYNTH";}
    if (value==5) {return "CALIBRATE";}
  }
}

void GUI_menu_up(int par) {
  tft.setFont(Terminal6x8);
  int menu_l;

  // cursor
  for (int i=0;i<m_size;i++) {
    tft.drawText(   x0,    int(2*ym/3)+10+10*i,   "  ");
  }
  
  tft.drawText(   x0,    int(2*ym/3)+10+10*cur,   ">");
    
}

void GUI_param_up(int n,int parame) {
  int col;
  if (CV_OUTS[mode][n].ch==0) {col=COLOR_YELLOW;}
  if (CV_OUTS[mode][n].ch==1) {col=COLOR_GREEN;}
  if (CV_OUTS[mode][n].ch==2) {col=COLOR_RED;}
  if (CV_OUTS[mode][n].ch==3) {col=COLOR_VIOLET;}
  if (CV_OUTS[mode][n].ch==4) {col=COLOR_CYAN;}

  if (parame == 0) {tft.drawText(   box_xy(n,1),    box_xy(n,0),   "CH"+String(CV_OUTS[mode][n].ch+1),col);}
  if (parame == 1) {tft.drawText(   box_xy(n,1),    box_xy(n,0)+10,   str(0,CV_OUTS[mode][n].msg,n),col);}
  if (parame == 2) {tft.drawText(   box_xy(n,1),    box_xy(n,0)+20,   "      ");tft.drawText(   box_xy(n,1),    box_xy(n,0)+20,   String(CV_OUTS[mode][n].ofst),col);}
  
}

void GUI_OUT_up() {
    for (int i=0;i<10;i++) {
    int col;
    if (CV_OUTS[mode][i].ch==0) {col=COLOR_YELLOW;}
    if (CV_OUTS[mode][i].ch==1) {col=COLOR_GREEN;}
    if (CV_OUTS[mode][i].ch==2) {col=COLOR_RED;}
    if (CV_OUTS[mode][i].ch==3) {col=COLOR_VIOLET;}
    if (CV_OUTS[mode][i].ch==4) {col=COLOR_CYAN;}

    tft.drawText(   box_xy(i,1),    box_xy(i,0),   "      ");
    tft.drawText(   box_xy(i,1),    box_xy(i,0)+20,   "      ");
    
    tft.drawText(   box_xy(i,1),    box_xy(i,0),   "CH"+String(CV_OUTS[mode][i].ch+1),col);
    tft.drawText(   box_xy(i,1),    box_xy(i,0)+10,   str(0,CV_OUTS[mode][i].msg,i),col);
    
    tft.drawText(   box_xy(i,1),    box_xy(i,0)+20,   String(CV_OUTS[mode][i].ofst),col);
    //tft.drawText(   box_xy(i,1),    box_xy(i,0)+30,   "GATE",col);
  }
}

void GUI_MAIN_init() {
  
  m_size=4;
  x0=x0-10;xm=xm+9;
  tft.drawRectangle(x0,    y0,           xm, int(2*ym/3), COLOR_WHITE);
  tft.drawRectangle(x0,    int(ym/3)+5, xm, int(2*ym/3), COLOR_WHITE);
  
  tft.drawRectangle(x0,    y0,           int(xm/5),   int(2*ym/3), COLOR_WHITE);
  tft.drawRectangle(x0,    y0,           int(2*xm/5), int(2*ym/3), COLOR_WHITE);
  tft.drawRectangle(x0,    y0,           int(3*xm/5), int(2*ym/3), COLOR_WHITE);
  tft.drawRectangle(x0,    y0,           int(4*xm/5), int(2*ym/3), COLOR_WHITE);
  x0=x0+10;xm=xm-9;
  tft.setFont(Terminal6x8);
  tft.drawText(   x0+10,    int(2*ym/3)+10,   "Mode :");
  tft.drawText(   x0+10,    int(2*ym/3)+20,   "Clock mult :");
  tft.drawText(   x0+10,    int(2*ym/3)+30,   "Edit outputs");
  tft.drawText(   x0+10,    int(2*ym/3)+40,   "Back");

  tft.drawText(   x0+100,    int(2*ym/3)+10,   str(1,mode));
  if (clockV[clockD]>0) {tft.drawText(   x0+102,    int(2*ym/3)+20,   "x"+String(abs(clockV[clockD])));}
  else {tft.drawText(   x0+102,    int(2*ym/3)+20,   "/" + String(abs(clockV[clockD])));}
  
  GUI_OUT_up();
}


void GUI_MAIN_loop() {

  while ( action != 99) {
    controls_read();
    COM_read();
     if (action == 1) {
        //tft.drawText(70,20,"^");
        if (cur>0) {cur_b=cur;cur--;}else {cur_b=cur;cur=m_size-1;}
     }
     if (action == 2) {
        //tft.drawText(70,20,"v");
        if (cur<m_size-1) {cur_b=cur;cur++;}else {cur_b=cur;cur=0;}
     }

     if (action == 3) { /////////////////////////////////////////////////  OPTIONS 
      blk=1;
      while (action == 3) {action=0;controls_read();} 
      boolean up_par=1;
       while (action == 0) {
        COM_read();     
        controls_read();
          switch(cur) {
            case 0: /////////////////////////////////////////////////  MODE
              if (action == 1) {
                if (mode<modeC) {mode++;}else{mode=0;}
                action = 0;up_par=1;
                COM_paramWrite(0,byte(mode),0);
              } else if (action == 2) {
                if (mode>0) {mode--;}else{mode=modeC;}
                action = 0;up_par=1;
                COM_paramWrite(0,byte(mode),0);
              }  else if (action == 3) {
              }
              
              if (up_par) {
                
                tft.drawText(   x0+100,    int(2*ym/3)+10+10*cur,   "                              ");
                tft.drawText(   x0+100,    int(2*ym/3)+10+10*cur,   str(1,mode));
                GUI_OUT_up();
                controls_read();
                while(action !=0) {action=0;controls_read();}delay(300);
                up_par=0;
              }
            break;
            case 1: ///////////////////////////////////////////////// CLOCK OUT           
              if (action == 1) {
                if (clockD<sizeof(clockV)/sizeof(clockV[0])-1) {clockD++;}else{clockD=0;}
                COM_paramWrite(2,byte(clockD),0);
                action = 0;up_par=1;
              } else if (action == 2) {
                if (clockD>0) {clockD--;}else{clockD=sizeof(clockV)/sizeof(clockV[0])-1;}
                COM_paramWrite(2,byte(clockD),0);
                action = 0;up_par=1;
              }  else if (action == 3) {
                
              } 
              if (up_par) {              
                tft.drawText(   x0+100,    int(2*ym/3)+10+10*cur,   "             ");
                if (clockV[clockD]>0) {tft.drawText(   x0+102,    int(2*ym/3)+20,   "x"+String(abs(clockV[clockD])));}
                else {tft.drawText(   x0+102,    int(2*ym/3)+20,   "/" + String(abs(clockV[clockD])));}
                controls_read();
                while(action !=0) {action=0;controls_read();}delay(300);
                up_par=0;
              }
            break;
            case 2: /////////////////////////////////////////////////  EDIT
              if (action == 1) {
                if (cbox<9) {cbox++;}else{cbox=0;}
                action = 0;up_par=1;
              } else if (action == 2) {
                if (cbox>0) {cbox--;}else{cbox=9;}
                action = 0;up_par=1;
              }  else if (action == 4) {
                  tft.drawRectangle(box_xy(cbox,1)-8,    box_xy(cbox,0)-8,           box_xy(cbox,1)+32,   box_xy(cbox,0)+38, COLOR_BLACK);
                  GUI_OUT_up();
                  //}
              } else if (action==3) { ////////////////////////////////// SELECT CV OUT
                //tft.drawText(   box_xy(cbox,1),    box_xy(cbox,0)+30,"X",COLOR_RED);
                tft.drawText(   50,    0,"hold button to exit",COLOR_WHITE);
                action=0;
                int pos=0,ttC=0;
                unsigned long tt=millis();
                while (action == 0) {
                  controls_read();
                  COM_read();
                   if (action == 2) {
                    GUI_param_up(cbox,pos);
                    if (pos<2) {pos++;}else{pos=0;}
                    action = 0;up_par=1;
                    }
                  if (action == 1) {
                    GUI_param_up(cbox,pos);
                    if (pos>0) {pos--;}else{pos=2;}
                    action = 0;up_par=1;
                  }
                  if (action == 3) { ////////////////////////////////// SELECT CV OUT PARAM
                    tft.drawText(   box_xy(cbox,1)-5,    box_xy(cbox,0)+pos*10,   ".");
                    tt=millis();
                    ttC=0;
                    int ofstL=500,incr=2;
                    if (mode == 5) {ofstL=7000;incr=1000;}
                    if (mode == 4) {ofstL=600;incr=2;}
                    while(action!=0) {action=0;controls_read();}
                    while(action==0) {
                      controls_read();
                      COM_read();
                       if (action == 1) {
                        if ((pos<2)&&(pos>=0)) {COM_CVchan(cbox,99);}
                        if (pos==0) {if(CV_OUTS[mode][cbox].ch<15){CV_OUTS[mode][cbox].ch++;} else {CV_OUTS[mode][cbox].ch=0;}GUI_OUT_up();}
                        if (pos==1) {if(CV_OUTS[mode][cbox].msg<10){CV_OUTS[mode][cbox].msg++;} else {CV_OUTS[mode][cbox].msg=0;}}
                        if (pos==2) {if(CV_OUTS[mode][cbox].ofst+incr<=ofstL){CV_OUTS[mode][cbox].ofst+=incr;} else {CV_OUTS[mode][cbox].ofst=0;}
                                     COM_paramWrite(10,cbox,map(CV_OUTS[mode][cbox].ofst,0,ofstL,0,255));}
                        if ((pos<2)&&(pos>=0)) {delay(50);COM_CVchan(cbox,cbox);}
                        action = 0;delay(300);up_par=1;
                        }
                      if (action == 2) {
                        if ((pos<2)&&(pos>=0)) {COM_CVchan(cbox,99);}
                        if (pos==0) {if(CV_OUTS[mode][cbox].ch>0){CV_OUTS[mode][cbox].ch--;} else {CV_OUTS[mode][cbox].ch=15;}GUI_OUT_up();}
                        if (pos==1) {if(CV_OUTS[mode][cbox].msg>0){CV_OUTS[mode][cbox].msg--;}else {CV_OUTS[mode][cbox].msg=10;}}
                        if (pos==2) {if(CV_OUTS[mode][cbox].ofst>=incr){CV_OUTS[mode][cbox].ofst-=incr;}else {CV_OUTS[mode][cbox].ofst=ofstL;}
                                     COM_paramWrite(10,cbox,map(CV_OUTS[mode][cbox].ofst,0,ofstL,0,255));}
                        if ((pos<2)&&(pos>=0)) {delay(50);COM_CVchan(cbox,cbox);}
                        action = 0;delay(300);up_par=1;
                      }

                      if (up_par) {
                        ttC=0;
                        up_par=0;
                        if (pos==0) {}
                        if (pos==1) {}
                        if (pos==2) {}
                      }      
                      
                      if (action == 3) {
                        //GUI_param_up(cbox,pos);
                        if (cbox<5) {tft.drawText(   box_xy(cbox,1)-5,    box_xy(cbox,0)+pos*10 ,   " ");}
                        blk=0;
                      }
                      
                      
                      if (ttC==0) {
                        if (millis()<tt+250) {/*GUI_OUT_up()*/GUI_param_up(cbox,pos);ttC=1;}
                        if (millis()>tt+500) {tt=millis();}                    
                      } else  {
                        if (millis()>tt+250) {tft.drawText(   box_xy(cbox,1),    box_xy(cbox,0)+pos*10,"      ",COLOR_YELLOW);ttC=0;} 
                      }
                      delay(5);
                    }
                    while(action!=0) {
                      action=0;controls_read();COM_read();
                    }
                    GUI_param_up(cbox,pos);
                  }
                  
                  if (action == 4) {  
                    tft.drawRectangle(box_xy(cbox,1)-8,    box_xy(cbox,0)-8,           box_xy(cbox,1)+32,   box_xy(cbox,0)+38, COLOR_BLACK);
                    blk=0;GUI_OUT_up();
                    tft.drawText(   50,    0,"                        ",COLOR_WHITE);           
                  }
                  
                  if (ttC==0) {
                    if (millis()<tt+250) {/*GUI_OUT_up()*/GUI_param_up(cbox,pos);ttC=1;}
                    if (millis()>tt+500) {tt=millis();} 
                  } else  {
                    if (millis()>tt+250) {tft.drawText(   box_xy(cbox,1),    box_xy(cbox,0)+pos*10,"      ",COLOR_YELLOW);ttC=0;}                   
                  }                    
                  if (up_par) {
                    ttC=0;
                    delay(100);  
                    up_par=0;
                  }         
                }
                GUI_OUT_up();            
              }
              if (up_par) {
                if (cbox!=0) {tft.drawRectangle(box_xy(cbox-1,1)-8,    box_xy(cbox-1,0)-8,           box_xy(cbox-1,1)+32,   box_xy(cbox-1,0)+38, COLOR_BLACK);}
                else {tft.drawRectangle(box_xy(9,1)-8,    box_xy(9,0)-8,           box_xy(9,1)+32,   box_xy(9,0)+38, COLOR_BLACK);}
                if (cbox!=9) {tft.drawRectangle(box_xy(cbox+1,1)-8,    box_xy(cbox+1,0)-8,           box_xy(cbox+1,1)+32,   box_xy(cbox+1,0)+38, COLOR_BLACK);}
                else {tft.drawRectangle(box_xy(0,1)-8,    box_xy(0,0)-8,           box_xy(0,1)+32,   box_xy(0,0)+38, COLOR_BLACK);}
                tft.drawRectangle(box_xy(cbox,1)-8,    box_xy(cbox,0)-8,           box_xy(cbox,1)+32,   box_xy(cbox,0)+38, COLOR_CYAN);
                controls_read();
                while(action !=0) {action=0;controls_read();}delay(300);
                up_par=0;
              }
            break;
          }
          
          if (blk !=0) {
            if (blk == 1) {blk=millis();tft.drawText(   x0,    int(2*ym/3)+10+10*cur,   ">");blkC=0;}
            if (millis()> blk+250) {if (blkC == 0) {tft.drawText(   x0,    int(2*ym/3)+10+10*cur,   "  ");blkC=1;}}
            if (millis()< blk+500 ){} else {blk=1;}
         }
      }
      blk=0;
     }
     
     if (action != 0) {
      GUI_menu_up(0);
      action=0;
      delay(200);
     }   
    
  }
  
}





void tft_init() {
  tft.begin();
  tft.setOrientation(3);
  tft.clear();
  tft.clear();
  xm=tft.maxX() - 10;
  ym=tft.maxY() - 10;
  
  if (!debug) {
    tft.setFont(Terminal6x8);
    char text[12]="           ";
    tft.drawText(10, 20, "Version 0.1");
    tft.setFont(Terminal12x16);
    tft.drawText(50, 50, "0x0cc17ecc");
    //delay(500);
    tft.drawText(55, 90, "Eurorack");
    tft.drawText(55, 110, "Midi 2 CV");
    delay(1000);
    tft.clear();
  }

  
}

/////////////////////////////////////// LEDS ///////////////////////////////////////////////////////////
void led_update(int n,int v,int s,boolean u) {
  int r,g,b=0;
  switch (s) {
    case 0:// 7F max bipolar
      if (v<64) {
        r=255*int((63-v)/63);
      } else {
        g=255*(v-64)/64;
      }
    break;
    case 1:// 7F max unipolar
      g=255*v/127;
    break;
    case 2:// MIDI Rx mode
      if ( ( (v & 0xF0) == 0x80) || ((v & 0xF0) == 0x90) ) {
        g=255;
      }
      if ((v & 0xF0) == 0xB0) {
        b=255;
      }
      if (v == 0xF8)  {
        r=255;
      }
    break;
  }

  
  strip.setPixelColor(n, r,g,b);
  if (u == 1) {
    strip.show();
  }
  
}



void led_reset() {
  for(int c=0; c<strip.numPixels(); c++) {
          strip.setPixelColor(c, 0,0,0);
  }
  strip.show(); 
}
void tftled_init() {
  strip.begin();
  strip.setBrightness(10);
  int d,e,f;
  tft.begin();
  tft.setOrientation(3);
  tft.clear();
  xm=tft.maxX() - 10;
  ym=tft.maxY() - 10;
  
  for (int i=0;i<5;i++) {
    if (i==0) {tft.setFont(Terminal6x8);char text[12]="           ";tft.drawText(10, 20, "Version 1.1");d=255;e=000;f=000;}
    if (i==1) {tft.setFont(Terminal12x16);tft.drawText(50, 50, "0x0cc17ecc");d=000;e=255;f=000;}
    if (i==2) {tft.drawText(55, 90, "Eurorack");tft.drawText(55, 110, "Midi 2 CV");d=000;e=000;f=255;}
    if (i==3) {d=255;e=255;f=255;}
    if (i==4) {d=0;e=0;f=0;}
    for(int c=0; c<strip.numPixels(); c++) {
          strip.setPixelColor(c, d,e,f);
        }
    strip.show(); 
    delay(500);
    
  }
  delay(500);
  tft.clear();
  
}

void led_init(boolean demo=1) {
  strip.begin();
  strip.setBrightness(25);
  int d,e,f;
  led_reset();
  strip.show();
  if (demo) {
    for (int i=0;i<5;i++) {
      if (i==0) {d=255;e=000;f=000;}
      if (i==1) {d=000;e=255;f=000;}
      if (i==2) {d=000;e=000;f=255;}
      if (i==3) {d=255;e=255;f=255;}
      if (i==4) {d=0;e=0;f=0;}
      for(int c=0; c<strip.numPixels(); c++) {
            strip.setPixelColor(c, d,e,f);
          }
      strip.show(); 
      delay(500);
    }
  }
  
 
}

void led_blink(int n,int t) {
  for (int i=0;i<n;i++) {
    digitalWrite(ledPin,HIGH);
    delay(t);
    digitalWrite(ledPin,LOW);
    delay(t);
  }
}                         
/////////////////////////////////////// SETUP & MAIN ///////////////////////////////////////////////////////////

void setup() {
  /*tft_init();
  led_init(0);*/
  controls_init();
  //EEPROM_init();
  Serial.begin(9600);
  tftled_init();
  //EEPROM_dump_strlist();
  /*COM_paramWrite(0,6,0);
  byte mmsg[]={3,0xD3,0,6};
  Serial.write(mmsg,4);*/
}

void loop() {
  //controls_read();
  GUI_MAIN_init();
  GUI_MAIN_loop();
}
