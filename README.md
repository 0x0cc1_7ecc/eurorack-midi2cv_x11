# Eurorack MIDI2CV_X11

An eurorack MIDI 2 CV module with 10 CV/GATE channels + 1 selectable clock
Features/parameters editing via a 3-button/2-RGB Leds/1-display GUI.
Uses 2 arduinos and 5 2-channel 12 bit DACs .

## Features
- 10 CV/Gate out channels, 12-Bit resolution, up to 7/8 volts
- Supports MIDI clock, gate, note, velocity and control change
- Live CV outs offset editing 
- waveform generation (sine,tri,sqr,distsine,multisine)
- 2 RGB Leds for MIDI IN/CLOCK OUT/MISC
- 1 Color display 



## Hardware
- 1 * Arduino pro micro -> MIDI to cv conversion
- 1 * Arduino nano 328p -> GUI Supports
- 5 * 2-Channel MC4822 12 bit DACs
- some TL072s/TL074s
- 1 * Optocoupler on MIDI input
- 2 * WS2812b Leds
- 1 *  ILI9225 TFT Screen
